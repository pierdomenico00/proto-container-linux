# Proto-container Linux from scratch
## *netcat* server listening on TCP port 80 running in a Linux pseudo container


Example in bash for direct use of Linux Kernel features to perform process isolation.
It runs a *netcat* server listening on port 80 exposed on corresponding host port, in his own network, ipc, mnt, uts and PID namespace. 
It's based on Ubuntu 16.04 filesystem copy, downloaded via *debootstrap*. File system handling via *btrfs-progs*.

### **Features used** :

* **chroot** (to emulate *pivot_root* use)
* **namespace** 
    - via *unshare* syscall, to run the process on a different ipc, mnt, uts and PID namespace.
    - via *iproute2* (*ip netns*) tool to run the process on a different network namespace
* **btrfs-progs** for filesystem handling

### **Features not used** :

* **cgroup** for hardware resources isolation
* **Security features** as:
    - **seccomp** to filter syscall use
    - **SELinux/AppArmor** for mandatory access control (MAC) security mechanism
    - **Linux Kernel capabilities** for fine-grained access control system
