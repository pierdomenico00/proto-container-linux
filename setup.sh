#!/bin/bash
##########################################
# Setup storage environment, btrfs based #
##########################################
echo "Storage environment setup..."
sudo funcs/filesystem.sh
###############################################
# Pull Ubuntu installation's file system copy #
###############################################
echo "Pullling Ubuntu installation's file system copy"
sudo funcs/ubuntu-pull.sh
#################
# Setup network #
#################
clear
echo "Setup network parameters..."
sudo funcs/networking.sh
echo "Network configured\n"
##########################################################
# Execute netcat server containerized on port 80 exposed #
##########################################################
# Create filesystem for container instance
sudo btrfs subvolume snapshot storage/rootfs storage/container
echo "Netcat server listening on port 80..."
sudo ip netns exec nscontainer unshare -fmuip chroot storage/container/ /bin/bash -c "nc -l 80"

