#!/bin/bash
###########################
# PRELIMINNARY OPERATIONS #
###########################
#Enable forwarding capability via kernel parameter
echo 1 > /proc/sys/net/ipv4/ip_forward
#Flush iptables rules
iptables --flush
#Mask everything leaves from bridge0
iptables -t nat -A POSTROUTING -o bridge0 -j MASQUERADE
#Mask evrything leaves from ens33
iptables -t nat -A POSTROUTING -o ens33 -j MASQUERADE
##########
# BRIDGE #
##########
#Setup a bridge network
ip link add bridge0 type bridge
ip addr add 10.0.0.1/24 dev bridge0
ip link set bridge0 up
########
# VETH #
########
#Setup veth device
ip link add dev veth0 type veth peer name veth1
#Enable veth0
ip link set dev veth0 up
#Connecto veth0 to bridge0 network
ip link set veth0 master bridge0
#Create network namespace named nscontainer
ip netns add nscontainer
#Connect veth1 to nscontainer network namespace
ip link set veth1 netns nscontainer
#Enable loopback interface
ip netns exec nscontainer ip link set dev lo up
#Set container's ip address
ip netns exec nscontainer ip addr add 10.0.0.2/24 dev veth1
#Enable veth1
ip netns exec nscontainer ip link set dev veth1 up
#Set container default gateway -> bridge0
ip netns exec nscontainer ip route add default via 10.0.0.1
###################
# PORT EXPOSITION #
###################
#Let first packet through the host firewall
iptables -A FORWARD -i ens33 -o bridge0 -p tcp --syn --dport 80 -m conntrack --ctstate NEW -j ACCEPT
#Allow subsequent traffic in both directions
iptables -A FORWARD -i ens33 -o bridge0 -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -i bridge0 -o ens33 -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
#Add NAT rules for redirection
iptables -t nat -A PREROUTING -i ens33 -p tcp --dport 80 -j DNAT --to-destination 10.0.0.2
iptables -t nat -A POSTROUTING -o bridge0 -p tcp --dport 80 -d 10.0.0.2 -j SNAT --to-source 10.0.0.1
