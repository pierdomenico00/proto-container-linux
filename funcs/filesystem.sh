#!/bin/bash

#########################
# FILESYSTEM OPERATIONS #
#########################

# Allocate an emtpy file of 3 GB
sudo fallocate -l 3G ./filesystem.img
# Format and mount on storage folder an emtpy btrfs filesystem
# on a loop pseudo device
mkdir ./storage
mkfs.btrfs ./filesystem.img
mount -o loop ./filesystem.img ./storage
# Create btrfs filesystem for image
sudo btrfs subvolume create ./storage/rootfs
