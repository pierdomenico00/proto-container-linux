#!/bin/bash
##########################################################################
# Pull a copy of Ubuntu installation's file system, without boot/ files  #
# debootstrap tool needed                                                #
##########################################################################
debootstrap --arch=amd64 xenial ./storage/rootfs http://archive.ubuntu.com/ubuntu/
